### Voxel based environment with chunk map in C

Librairies used :
* SDL
* OpenGL
* stb_image

How to build :
1. cmake .
2. make

##### [Video link](https://youtu.be/ly_38qfTG24)
##### [Video link with chunks](https://youtu.be/NxJxfTvA0zM)
